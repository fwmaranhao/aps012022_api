from django.urls import path, re_path
from rest_framework.routers import SimpleRouter

from . import views

from .views import (PontoViewSet)

router = SimpleRouter()

router.register('pontos', PontoViewSet)

urlpatterns = [
    #path('', views.focos, name='focos'),
    #re_path('^login/(?P<username>.+)/(?P<senha>.+)/$', login.as_view()),
    #path('rota', views.rotas, name='rota'),
    #path('painel', views.processaLogin, name='painel'),
]