from rest_framework import serializers

from .models import Ponto


class PontoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ponto
        fields = (
		    'id',	
            'titulo',
            'tipo',
            'latitude',
            'longitude',
            'status',
        )
        datatables_always_serialize = (
		    'id',
            'titulo',
            'tipo',
            'latitude',
            'longitude',
            'status',
        )