from django.shortcuts import render

from rest_framework import generics
from rest_framework import viewsets

from app.models import Ponto
from .serializer import PontoSerializer

# Create your views here.

class PontoViewSet(viewsets.ModelViewSet):
    queryset = Ponto.objects.all()
    serializer_class = PontoSerializer