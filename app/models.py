from django.db import models

# Create your models here.

class Ponto(models.Model):
    titulo = models.TextField()
    tipo = models.TextField()
    latitude = models.TextField()
    longitude = models.TextField()
    status = models.TextField()